import React from 'react'
import Navigation from './components/navigation/Navigation'
import Routers from './components/routers/Routers'

const App = () => {
  return (
    <div>
      <Navigation></Navigation>
      <Routers></Routers>
    </div>
  )
}

export default App
