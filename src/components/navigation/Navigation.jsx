import axios from 'axios'
import React from 'react'
import { NavLink, useNavigate } from 'react-router-dom'
import { baseUrl } from '../../config/config'
import { getLoginInfo, removeLoginInfo } from '../../utils/loginInfo'

const Navigation = () => {
  let navigate = useNavigate()
  let logout = async () => {
    try {
      await axios({
        url: `${baseUrl}/admin/logout`,
        method: 'POST',
        headers: {
          Authorization: `Bearer ${getLoginInfo().token}`,
        }
      })
      removeLoginInfo()
      navigate('admin/login')
    } catch (error) {
      console.log("Unable to Logout");
    }
  }
  return (
    <div>
      <div>
        <nav className='bg-gray-600 p-4 flex justify-between text-white'>
          <div className='flex justify-start gap-4  w-max p-1'>
            <NavLink className="flex justify-start hover:translate-x-0.5" to="/admin">Admin-List</NavLink>
            <NavLink className="flex justify-start hover:translate-x-0.5" to="/admin/my-profile">Admin-Profile</NavLink>
          </div>
          <div className='flex justify-between gap-3'>
            <NavLink className="bg-green-600 rounded p-2 flex justify-start hover:translate-x-0.5" to="/admin/login">Login</NavLink>

            <button onClick={() => {
              logout()
            }} className="bg-red-700 rounded p-2 text-sm cursor-pointer">logout</button>
          </div>

        </nav>

      </div>
    </div>
  )
}

export default Navigation
