import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { baseUrl } from '../../config/config'
import { getLoginInfo } from '../../utils/loginInfo'
import { useNavigate } from 'react-router-dom'

const AdminProfile = () => {
  let [adminProfile, setAdminProfile] = useState({
    firstName: "",
    lastName: "",
    middleName: "",
    password: "",
    email: "",
    dob: "",
    gender: "",
    role: "",
    phoneNumber: "",
  })
  let navigate = useNavigate()
  let readProfile = async () => {

    try {
      let response = await axios({
        url: `${baseUrl}/admin/my-profile`,
        method: 'GET',
        headers: {
          Authorization: `Bearer ${getLoginInfo()?.token}`
        }
      })
      setAdminProfile(response.data.data)
      
    } catch (error) {
      
    }

    

  }
  useEffect(() => {
    readProfile()
  }, [])

  return (
    <div style={{ 'height': '641px' }} className='bg-yellow-900 flex items-center justify-center'>
      <div className='bg-cyan-500 p-2 rounded'>
        <div
          className="block rounded-lg bg-white shadow-[0_2px_15px_-3px_rgba(0,0,0,0.07),0_10px_20px_-2px_rgba(0,0,0,0.04)] dark:bg-neutral-700">
          <div className="p-6">
            <h5
              className="mb-2 text-xl font-medium leading-tight text-neutral-800 dark:text-neutral-50">
              USER PROFILE
            </h5>
            <ul className="mb-8 space-y-4 text-left text-gray-500 dark:text-gray-400">
              <li className="flex items-center space-x-3">
                {/* <!-- Icon --> */}
                <svg className="flex-shrink-0 w-5 h-5 text-green-500 dark:text-green-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd"></path></svg>
                <span>First Name : <span className="font-semibold text-gray-900 dark:text-white">{adminProfile?.firstName}</span></span>
              </li>
              <li className="flex items-center space-x-3">
                {/* <!-- Icon --> */}
                <svg className="flex-shrink-0 w-5 h-5 text-green-500 dark:text-green-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd"></path></svg>
                <span>Middle Name : <span className="font-semibold text-gray-900 dark:text-white">{adminProfile?.middleName}</span> </span>
              </li>
              <li className="flex items-center space-x-3">
                {/* <!-- Icon --> */}
                <svg className="flex-shrink-0 w-5 h-5 text-green-500 dark:text-green-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd"></path></svg>
                <span>Last Name : <span className="font-semibold text-gray-900 dark:text-white">{adminProfile?.lastName}</span></span>
              </li>
              <li className="flex items-center space-x-3">
                {/* <!-- Icon --> */}
                <svg className="flex-shrink-0 w-5 h-5 text-green-500 dark:text-green-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd"></path></svg>
                <span>Email : <span className="font-semibold text-gray-900 dark:text-white">{adminProfile?.email}</span></span>
              </li>
              <li className="flex items-center space-x-3">
                {/* <!-- Icon --> */}
                <svg className="flex-shrink-0 w-5 h-5 text-green-500 dark:text-green-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd"></path></svg>
                <span>Date of Birth : <span className="font-semibold text-gray-900 dark:text-white">{adminProfile?.dob}</span></span>
              </li>
              <li className="flex items-center space-x-3">
                {/* <!-- Icon --> */}
                <svg className="flex-shrink-0 w-5 h-5 text-green-500 dark:text-green-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd"></path></svg>
                <span>Gender : <span className="font-semibold text-gray-900 dark:text-white">{adminProfile?.gender}</span></span>
              </li>
              <li className="flex items-center space-x-3">
                {/* <!-- Icon --> */}
                <svg className="flex-shrink-0 w-5 h-5 text-green-500 dark:text-green-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd"></path></svg>
                <span>Role : <span className="font-semibold text-gray-900 dark:text-white">{adminProfile?.role}</span></span>
              </li>
              <li className="flex items-center space-x-3">
                {/* <!-- Icon --> */}
                <svg className="flex-shrink-0 w-5 h-5 text-green-500 dark:text-green-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M16.707 5.293a1 1 0 010 1.414l-8 8a1 1 0 01-1.414 0l-4-4a1 1 0 011.414-1.414L8 12.586l7.293-7.293a1 1 0 011.414 0z" clip-rule="evenodd"></path></svg>
                <span>Phone Number : <span className="font-semibold text-gray-900 dark:text-white">{adminProfile?.phoneNumber}</span></span>
              </li>
            </ul>
          </div>
        </div>
        <div className='flex justify-between p-4'>
          <div><button onClick={() => { navigate('/admin/update-profile') }} className="bg-yellow-200 p-2 rounded">Edit Profile</button></div>
          <div><button onClick={() => { navigate('/admin/update-password') }} className="bg-yellow-300 p-2 rounded">Update Password</button></div>
        </div>
      </div>

    </div>
  )
}

export default AdminProfile
