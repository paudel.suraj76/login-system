import React, { useState } from 'react'
import { getLoginInfo, removeLoginInfo } from '../../utils/loginInfo'
import axios from 'axios'
import { baseUrl } from '../../config/config'
import { useNavigate } from 'react-router-dom'

const UpdatePassword = () => {
    let navigate = useNavigate()
    // let oldPass = getLoginInfo().user.password
    // let [passwordInfo, setPasswordInfo] = useState({
    //     'oldPasswords': "",
    //     'newPasswords': "",
    //     'confirmPassword': "",
    // })
    let [password,setPassword]=useState("")

    let updateNewPassword = async () => {
        try {
            await axios({
                url: `${baseUrl}/admin/update-password`,
                method: 'patch',
                data: { 'password':password },
                headers:{
                    Authorization:`Bearer ${getLoginInfo().token}`
                }
            })
            console.log("password changed successfully")
            removeLoginInfo()
            navigate(`/admin/login`)
        } catch (error) {
            console.log("error: " + error)
        }

    }


  
    // let checkPassword = () => {
    //     if (oldPass === setPasswordInfo.oldPasswords) {
    //         if (passwordInfo.newPasswords === passwordInfo.confirmPassword) {
    //             // updateNewPassword()
    //         }
    //     }
    // }

    return (
        <div style={{ 'height': '641px' }} className='flex items-center justify-center bg-gray-500'>

            <div className='admin-register-form m-5 border border-black bg-gray-200 p-4 w-6/12 h-54 rounded-md'>
                <div className='mb-2 text-lg font-bold text-purple-950 underline'>Update Password</div>
                <form onSubmit={(e) => {
                    e.preventDefault()
                    updateNewPassword()

                }} className='grid grid-cols-1'>

                    {/* <div className='mb-2 grid'>
                        <label htmlFor='oldPass' className='font-bold text-purple-950'>Old Password</label>
                        <input value={passwordInfo.oldPasswords} onChange={(e) => {
                            setPasswordInfo((prev) => {
                                return { ...prev, 'oldPassword': e.target.value }
                            })
                        }} id="oldPass" type="password" className='p-2 ml-1 rounded border border-black'></input>
                    </div> */}

                    <div className='mb-2 grid'>
                        <label htmlFor='currentPassword' className='font-bold text-purple-950'>New Password</label>
                        <input value={password} onChange={(e) => {
                            setPassword(e.target.value);
                        }} id="currentPassword" type="password" className='p-2 ml-1 rounded border border-black'></input>
                    </div>

                    {/* <div className='mb-2 grid'>
                        <label htmlFor='conPassword' className='font-bold text-purple-950'>Confirm Password</label>
                        <input value={passwordInfo.confirmPassword} onChange={(e) => {
                            setPasswordInfo((prev) => {
                                return { ...prev, 'confirmPassword': e.target.value }
                            })
                        }} id="conPassword" type="password" className='p-2 ml-1 rounded border border-black'></input>
                    </div> */}

                    <div className='mb-2 flex justify-start'>
                        <input type="submit" value="Update" className='cursor-pointer bg-green-800 text-white p-2 ml-1 rounded border border-black'></input>
                    </div>
                </form>

            </div>
        </div>
    )
}

export default UpdatePassword
