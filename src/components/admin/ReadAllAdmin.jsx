import axios from 'axios'
import React, { useEffect, useState } from 'react'
import {baseUrl} from '../../config/config.js'
import { getLoginInfo } from '../../utils/loginInfo.js'

const ReadAllAdmin = () => {
  let [adminInfo,setAdminInfo]=useState(null)
  let [isLoading,setIsLoading]=useState(true)
  let [isError,setIsError]=useState("")

  let readAllAdmin=async()=>{
    try {
      let response = await axios({
        url: `${baseUrl}/admin`,
        method: "GET",
        headers: {
          Authorization: `Bearer ${getLoginInfo().token}`
        }
      })
      setAdminInfo(response.data.data.results)
      setIsLoading(false)
    } catch (error) {
      setIsLoading(false)
      setIsError("Error")
      
    }

  
  }
  useEffect(()=>{readAllAdmin()},[baseUrl])

  return (
    <div>
      <div className="bg-yellow-200 flex items-center justify-center text-white">
        <table className="mt-2 mb-2 min-w-fit text-left text-sm font-light table-auto">
          <thead className="border-b bg-slate-950 font-medium dark:border-neutral-500">
            <tr>
              <th scope="col" className="px-6 py-4 w-auto">SN</th>
              <th scope="col" className="px-6 py-4 w-auto">First Name</th>
              <th scope="col" className="px-6 py-4 w-auto">Middle Name</th>
              <th scope="col" className="px-6 py-4 w-auto">Last Name</th>
              <th scope="col" className="px-6 py-4 w-auto">Email</th>
              <th scope="col" className="px-6 py-4 w-auto">DOB</th>
              <th scope="col" className="px-6 py-4 w-auto">Gender</th>
              <th scope="col" className="px-6 py-4 w-auto">Role</th>
              <th scope="col" className="px-6 py-4 w-auto">Phone Number</th>
              <th scope="col" className="px-6 py-4 w-auto">Operation</th>
            </tr>
          </thead>
          <tbody className='bg-purple-400 text-slate-950'>
            {isLoading ? <tr><td colSpan="10">loading....</td></tr> : null}
            {isError === "" ? null : <tr><td colSpan="10">{isError}</td></tr>}
            {adminInfo && adminInfo.map((item, i) => (
              <tr key={i} className="border-b dark:border-neutral-500">
                <td className="whitespace-nowrap px-6 py-4 font-medium">{i + 1}</td>
                <td className="whitespace-nowrap px-6 py-4 w-auto">{item?.firstName}</td>
                <td className="whitespace-nowrap px-6 py-4 w-auto">{item?.middleName}</td>
                <td className="whitespace-nowrap px-6 py-4 w-auto">{item?.lastName}</td>
                <td className="whitespace-nowrap px-6 py-4 w-auto">{item?.email}</td>
                <td className="whitespace-nowrap px-6 py-4 w-auto">{item?.dob}</td>
                <td className="whitespace-nowrap px-6 py-4 w-auto">{item?.gender}</td>
                <td className="whitespace-nowrap px-6 py-4 w-auto">{item?.role}</td>
                <td className="whitespace-nowrap px-6 py-4 w-auto">{item?.phoneNumber}</td>
                <td className="whitespace-nowrap px-6 py-4 w-auto">btn</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>
  )
}

export default ReadAllAdmin
