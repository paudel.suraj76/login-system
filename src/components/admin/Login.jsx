import React, { useState } from 'react'
import { baseUrl } from '../../config/config'
import axios from 'axios'
import { setLoginInfo } from '../../utils/loginInfo'
import { NavLink, useNavigate } from 'react-router-dom'

const Login = () => {
  let navigate=useNavigate()
  let [credential,setCredential]=useState({
    'email':"",
    'password':"",
  })
  let AdminLogin = async () => {
    try {
      let response = await axios({
        url: `${baseUrl}/admin/login`,
        method: 'POST',
        data: credential
      })
      setLoginInfo(response.data.data)
      navigate('/admin')
      
    } catch (error) {
      console.log("Unable to login",error)
    }

  }
  return (
    <div style={{'height':'641px'}} className='flex items-center justify-center bg-gray-500'>
      <div className='admin-register-form m-5 border border-black bg-gray-200 p-4 w-6/12 h-54 rounded-md'>
        <form onSubmit={(e)=>{
                      e.preventDefault()
                      AdminLogin()
        }} className='grid grid-cols-1'>

          <div className='mb-2 grid'>
            <label htmlFor='email' className='font-bold text-purple-950'>Email</label>
           <input value={credential.email} onChange={(e)=>{
            setCredential((prev)=>{
              return {...prev,'email':e.target.value}
            })
           }} id="email" type="email" className='p-2 ml-1 rounded border border-black'></input>
          </div>

          <div className='mb-2 grid'>
            <label htmlFor='password' className='font-bold text-purple-950'>Password</label>
           <input value={credential.password} onChange={(e)=>{
            setCredential((prev)=>{
              return {...prev,'password':e.target.value}
            })
           }} id="password" type="password" className='p-2 ml-1 rounded border border-black'></input>
          </div>

          <div className='mb-2 flex justify-start'>
            <input type="submit" value="Login" className='cursor-pointer bg-green-800 text-white p-2 ml-1 rounded border border-black'></input>
          </div>
        </form>
        <small>if you have't register yet : </small><NavLink className="font-bold text-lg" to="/admin/register">Register Here !</NavLink>
      </div>
    </div>
  )
}

export default Login
