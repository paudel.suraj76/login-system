import axios from 'axios'
import React, {  useState } from 'react'
import { baseUrl } from '../../config/config'
import { useNavigate } from 'react-router-dom'

const AdminRegister = () => {
  let navigate=useNavigate()
  let roles = [
    { label: "Select one", value: "", disabled: true },
    { label: "Admin", value: "admin" },
    { label: "Super-Admin", value: "superAdmin" }
  ]

  let genders = [
    { label: "Male", value: "male" },
    { label: "Female", value: "female" },
    { label: "Other", value: "other" },
  ]
  let [adminInfo, setAdminInfo] = useState({
    firstName: "",
    lastName: "",
    middleName: "",
    password: "",
    email: "",
    dob: "",
    gender: "",
    role: "",
    phoneNumber: "",
  })
  
  let sendData = async () => {
    try{
      let response = await axios({
        url: `${baseUrl}/admin/register`,
        method: 'POST',
        data: adminInfo
      })
      console.log("success", response)
      navigate('/admin/login')
    }catch(error){
      console.log(error)
    }


  }
  let Submitted = async (e) => {
    e.preventDefault();
    sendData()
  }
console.log(adminInfo)

  return (
    <div>
      <div className='admin-register-form m-5'>
        <form onSubmit={Submitted}>
          <div className='grid grid-cols-4 gap-2 border border-black p-5 content-end bg-gray-50'>


            <div className='mb-2'>
              <label htmlFor='fn' className='grid grid-cols-1 content-center font-bold text-purple-950'>First Name</label>
              <input onChange={(e) => {
                setAdminInfo((prev) => {
                  return { ...prev, 'firstName': e.target.value }
                })
              }} value={adminInfo.firstName} id="fn" type="text" className='p-2 ml-1 rounded border border-black'></input>
            </div>

            <div className='mb-2'>
              <label htmlFor='mdn' className='grid grid-cols-1 content-center font-bold text-purple-950'>Middle Name</label>
              <input onChange={(e) => {
                setAdminInfo((prev) => {
                  return { ...prev, 'middleName': e.target.value }
                })
              }} value={adminInfo.middleName} id="mdn" type="text" className='p-2 ml-1 rounded border border-black'></input>
            </div>

            <div className='mb-2'>
              <label htmlFor='ln' className='grid grid-cols-1 content-center font-bold text-purple-950'>Last Name</label>
              <input onChange={(e) => {
                setAdminInfo((prev) => {
                  return { ...prev, 'lastName': e.target.value }
                })
              }} value={adminInfo.lastName} id="ln" type="text" className='p-2 ml-1 rounded border border-black'></input>
            </div>

            <div className='mb-2'>
              <label htmlFor='email' className='grid grid-cols-1 content-center font-bold text-purple-950'>Email</label>
              <input onChange={(e) => {
                setAdminInfo((prev) => {
                  return { ...prev, 'email': e.target.value }
                })
              }} value={adminInfo.email} id="email" type="email" className='p-2 ml-1 rounded border border-black'></input>
            </div>

            <div className='mb-2'>
              <label htmlFor='password' className='grid grid-cols-1 content-center font-bold text-purple-950'>Password</label>
              <input onChange={(e) => {
                setAdminInfo((prev) => {
                  return { ...prev, 'password': e.target.value }
                })
              }} value={adminInfo.password} id="password" type="password" className='p-2 ml-1 rounded border border-black'></input>
            </div>

            <div className='mb-2'>
              <label htmlFor='dob' className='grid grid-cols-1 content-center font-bold text-purple-950'>Date of Birth</label>
              <input onChange={(e) => {
                setAdminInfo((prev) => {
                  return { ...prev, 'dob': e.target.value }
                })
              }} value={adminInfo.dob} id="dob" type="date" className='p-2 ml-1 rounded border border-black'></input>
            </div>

            <div className='mb-2'>
              <label htmlFor='phn' className='grid grid-cols-1 content-center font-bold text-purple-950'>Phone</label>
              <input onChange={(e) => {
                setAdminInfo((prev) => {
                  return { ...prev, 'phoneNumber': e.target.value }
                })
              }} value={adminInfo.phoneNumber} id="phn" type="number" className='p-2 ml-1 rounded border border-black'></input>
            </div>

            <div className='mb-2'>
              <label htmlFor='phn' className='grid grid-cols-1 content-center font-bold text-purple-950'>Select Role</label>
              <select onChange={(e) => {
                setAdminInfo((prev) => {
                  return { ...prev, 'role': e.target.value }
                })
              }} value={adminInfo.role} className='p-2 ml-1 rounded border border-black'>
                {
                  roles.map((item, i) => {
                    return <option key={i} value={item.value} disabled={item.disabled} >{item.label}</option>
                  })
                }

              </select>
            </div>

            <div className='mb-2'>
              <label htmlFor='gender' className='grid grid-cols-1 content-center font-bold text-purple-950'>Gender</label>
              {
                genders.map((item, i) => {
                  return <div>
                    <label htmlFor='gender' className='text-black'>{item.label}</label>
                    <input onChange={(e) => {
                      setAdminInfo((prev) => {
                        return { ...prev, 'gender': e.target.value }
                      })
                    }} checked={adminInfo.gender === item.value} value={item.value} id="gender" type="radio" className='p-2 ml-1 rounded border border-black'></input>
                  </div>
                })
              }
            </div>


          </div>
          <div className='mb-2 mt-2 flex justify-start'>

            <input type="submit" className='bg-green-800 text-white p-2 ml-1 rounded border border-black cursor-pointer'></input>
          </div>
        </form>
      </div>
    </div>
  )
}

export default AdminRegister
