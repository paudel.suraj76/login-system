import React from 'react'
import { Route, Routes } from 'react-router-dom'
import AdminRegister from '../admin/AdminRegister'
import Login from '../admin/Login'
import ReadAllAdmin from '../admin/ReadAllAdmin'
import AdminProfile from '../admin/AdminProfile'
import UpdatePassword from '../admin/UpdatePassword'
import UpdateAdminProfile from '../admin/UpdateAdminProfile'

const Routers = () => {
  return (
    <div>
      <Routes>
        <Route path='/admin/register' element={<AdminRegister></AdminRegister>}></Route>
        <Route path='/admin/login' element={<Login></Login>}></Route>
        <Route path='/admin' element={<ReadAllAdmin></ReadAllAdmin>}></Route>
        <Route path='/admin/my-profile' element={<AdminProfile></AdminProfile>}></Route>
        <Route path='/admin/update-password' element={<UpdatePassword></UpdatePassword>}></Route>
        <Route path='/admin/update-profile' element={<UpdateAdminProfile></UpdateAdminProfile>}></Route>
      </Routes>
    </div>
  )
}

export default Routers
