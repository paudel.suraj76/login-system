// after login to maintain the login session we store the data on local storage as below.
export let setLoginInfo = (data)=>{
    localStorage.setItem('info',JSON.stringify(data))
}

// to get data from local storage
export let getLoginInfo=()=>{
        let userData=localStorage.getItem("info")
        let parsedUserData=JSON.parse(userData)
        return parsedUserData
}

// to remove data from localStorage
export let removeLoginInfo=()=>{
    localStorage.removeItem("info")
}